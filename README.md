# Streamlit Application with embedded Google Flan LLM
![alt text](readme_images/StreamlitLLMdemo.gif)

This is a Streamlit application which uses a Hugging Face API to 
embed a chatbot. In this instance, we have used [Google Flan](https://huggingface.co/google/flan-t5-xxl)
to provide an interesting chatbot experience. Choosing this model lets us show off some interesting things.
First, this model is small enough to be called directly with the Hugging Face free Inference API (serverless) deployment option.
This is really great for experimentation and prototyping because you don't have to establish a dedicated (paid) endpoint
with a cloud provider to serve the model. Second, unlike some of the other freely available models, Google Flan performs quite
well at a variety of NLP tasks.

Streamlit is an intuitive web application development framework that lets you take python code directly to a 
web application with an easy to learn syntax. Those who have used frameworks like Flask and Actix may be familiar with css styling,javascript, and the general complexity of web application development but Streamlit is all in one .py file and uses a combination of python and markdown to generate complex web application (if you don't believe me just check the [gitlab repo](https://gitlab.com/johncoogan53/streamlit_llm))

A detailed paper on [Google Flan](https://arxiv.org/pdf/1910.10683.pdf) discusses how the model was pretrained on many tasks
and as a result, performs well against downstream benchmarks for Machine Translation, Question Answering, Abstractive Summarization, and Text Classification. It does this by including the task in the prompt itself. Appendix D of this paper describes what these preprocessed prompts look like. One of the most interesting behaviors is to ask a question and state to give the rationale before answering.

## CI/CD Notes:

This project containerizes the Streamlit application as a docker image and saves that image in a .tar format as a build artifact. This .tar file can be loaded:

![alt text](readme_images/buildatr.png)

>docker load -i streamlit_llm.tar

and then run:

>docker run -p 8501:8501 --env-file ./.env streamlit_llm

This assumes you have a .env file in that directory that contains your Hugging Face API token: 

>.env
![alt text](<readme_images/Screenshot 2024-04-07 175521.png>)

This is a way to pass environment secrets to a docker image at runtime which allows you to separate sensitive information from your docker images which can be inspected by anyone accessing the container. 


