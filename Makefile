IMAGE_NAME = streamlit_llm

.PHONY: build
build:
	docker build -t $(IMAGE_NAME) .

.PHONY: check
check:
	docker images

.PHONY: run
run:
	@echo $(IMAGE_NAME)
	docker run -p 8501:8501 --env-file ./.env $(IMAGE_NAME)




