import streamlit as st
import requests
import os

API_URL = "https://api-inference.huggingface.co/models/google/flan-t5-xxl"
token = os.environ["HUGGING_FACE_TOKEN"]

HEADERS = {"Authorization": f"Bearer {token}"}

def query(payload):
    response = requests.post(API_URL, headers=HEADERS, json=payload)
    return response.json()[0]["generated_text"]  # Extract the generated text from the response

def main():
    st.title("Streamlit Chatbot with Google Flan")

    # Create two columns layout
    col1, col2 = st.columns([4, 3])

    # Column 1: Description of the chatbot
    with col1:
        st.header("About the Project")
        st.write("""This is a Streamlit application which uses a Hugging Face API to 
        embed a chatbot. In this instance, we have used [Google Flan](https://huggingface.co/google/flan-t5-xxl)
        to provide an interesting chatbot experience. Choosing this model lets us show off some interesting things.
        First, this model is small enough to be called directly with the Hugging Face free Inference API (serverless) deployment option.
        This is really great for experimentation and prototyping because you don't have to establish a dedicated (paid) endpoint
        with a cloud provider to serve the model. Second, unlike some of the other freely available models, Google Flan performs quite
        well at a variety of NLP tasks.""")

        st.header("Why Streamlit?")
        st.write("""Streamlit is an intuitive web application development framework that lets you take python code directly to a 
        web application with an easy to learn syntax. Those who have used frameworks like Flask and Actix may be familiar with css styling,
        javascript, and the general complexity of web application development but Streamlit is all in one .py file and uses a combination of python
        and markdown to generate complex web application (if you don't believe me just check the [gitlab repo](https://gitlab.com/johncoogan53/streamlit_llm))""")

        st.header("Try it out to the right!")
    # Column 2: Chatbot interface
    with col2:
        st.header("More about Google Flan")
        st.write("""A detailed paper on [Google Flan](https://arxiv.org/pdf/1910.10683.pdf) discusses how the model was pretrained on many tasks
        and as a result, performs well against downstream benchmarks for Machine Translation, Question Answering, Abstractive Summarization,
        and Text Classification. It does this by including the task in the prompt itself. Appendix D of this paper describes what these preprocessed
        prompts look like. One of the most interesting behaviors is to ask a question and state to give the rationale before answering.""")
        # Initialize conversation history
        conversation_history = []

        # Create text input box for user input
        user_input = st.text_area("You:", height=100)

        # If user submits a message
        if st.button("Send"):
            if user_input:
                # Add user input to conversation history
                conversation_history.append({"role": "user", "message": user_input})

                # Get chatbot response
                bot_response = query({"inputs": user_input})

                # Add bot response to conversation history
                conversation_history.append({"role": "bot", "message": bot_response})

        # Display conversation history
        for entry in conversation_history:
            if entry["role"] == "bot":
                st.text_area("Chatbot:", value=entry["message"], key=entry["message"], disabled=True)

if __name__ == "__main__":
    main()
